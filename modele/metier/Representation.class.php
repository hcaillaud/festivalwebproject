<?php

namespace modele\metier;

/**
 * Description of Representation
 *
 * @author btssio
 */
class Representation {

    /**
     * id de la représentation (R***)
     * @var String
     */
    private $idRepresentation;
    
    /**
     * Lieu de la représentation
     * @var Lieu
     */
    private $Lieu;
    
    /**
     * Groupe se représentant
     * @var Groupe
     */
    private $Groupe;
    
    /**
     * date de la représentation
     * @var date
     */
    private $date;
    
    /**
     * heure de début de la représentation
     * @var Integer
     */
    private $HDebut;
    
    /**
     * heure de fin de la représentation
     * @var Integer
     */
    private $HFin;
    
    /**
     * Constructeur de Representation
     * @param \modele\metier\String $idRepresentation
     * @param \modele\metier\Lieu $Lieu
     * @param \modele\metier\Groupe $Groupe
     * @param \modele\metier\date $date
     * @param \modele\metier\String $HDebut
     * @param \modele\metier\String $HFin
     */
    function __construct($idRepresentation, $Lieu, $Groupe, $date, $HDebut, $HFin) {
        $this->idRepresentation = $idRepresentation;
        $this->Lieu = $Lieu;
        $this->Groupe = $Groupe;
        $this->date = $date;
        $this->HDebut = $HDebut;
        $this->HFin = $HFin;
    }
    //ACCESSEURS ET MUTATEURS
    function getIdRepresentation(): String {
        return $this->idRepresentation;
    }

    function getLieu(): Lieu {
        return $this->Lieu;
    }

    function getGroupe(): Groupe {
        return $this->Groupe;
    }

    function getDate(){
        return $this->date;
    }

    function getHDebut(): String {
        return $this->HDebut;
    }

    function getHFin(): String {
        return $this->HFin;
    }

    function setIdRepresentation(String $idRepresentation) {
        $this->idRepresentation = $idRepresentation;
    }

    function setLieu(Lieu $Lieu) {
        $this->Lieu = $Lieu;
    }

    function setGroupe(Groupe $Groupe) {
        $this->Groupe = $Groupe;
    }

    function setDate(date $date) {
        $this->date = $date;
    }

    function setHDebut(String $HDebut) {
        $this->HDebut = $HDebut;
    }

    function setHFin(String $HFin) {
        $this->HFin = $HFin;
    }
}
