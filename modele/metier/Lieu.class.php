<?php


namespace modele\metier;

/**
 * Description of Lieu
 *
 * @author hcaillaud
 */
class Lieu {
    /**
     * id du lieu
     * @var String
    */
    private $IdLieu;
    /**
     * nom du lieu
     * @var string
     */
    private $Nom;
    /**
     * adresse du lieu
     * @var string
     */
    private $Adresse;
    /**
     * capacite du lieu
     * @var integer
     */
    private $CapAccueil;

    function __construct($id, $nom, $adresse, $capAccueil) {
        $this->IdLieu = $id;
        $this->Nom = $nom;
        $this->Adresse = $adresse;
        $this->CapAccueil = $capAccueil;
    }


    // ACCESSEURS ET MUTATEURS
    function getId() {
        return $this->IdLieu;
    }
            
    function getNom() {
        return $this->Nom;
    }

    function getAdresse() {
        return $this->Adresse;
    }

    function getCapAccueil() {
        return $this->CapAccueil;
    }
    
    function setId($id) {
        $this->IdLieu = $id;
    }

    function setNom($nom) {
        $this->Nom = $nom;
    }

    function setAdresse($adresse) {
        $this->Adresse = $adresse;
    }

    function setCapAccueil($capAccueil) {
        $this->CapAccueil = $capAccueil;
    }


}
