<?php
/**
 * Description of CtrlRepresentation
 *
 * @author btssio
 */

namespace controleur;

use modele\dao\RepresentationDao;
use modele\dao\GroupeDAO;
use modele\dao\LieuDAO;
use modele\metier\Representation;
use modele\metier\Lieu;
use modele\metier\Groupe;
use modele\dao\Bdd;
use vue\representation\VueConsultationRepresentation;
use vue\representation\VueSaisieRepresentation;
use vue\representation\VueSupprimmerRepresentation;



class CtrlRepresentation extends ControleurGenerique {
    /** controleur= representation & action= defaut
     * Afficher la liste des offres d'hébergement      */
    public function defaut() {
        $this->consulter();
    }

    /** controleur= representation & action= consulter
     * Afficher la liste des Représenttions      */
    function consulter() {
        $laVue = new VueConsultationRepresentation();
        $this->vue = $laVue;
        // La vue a besoin de la liste des Représentations
        Bdd::connecter();
        $laVue->setLesRepresentations(RepresentationDAO::getAll());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representation");
        $this->vue->setVersion($this->version);
        $this->vue->afficher();
    }
    
    /** controleur= Representation & action=creer
     * Afficher le formulaire d'ajout d'une Representation     */
    public function creer() {
        $laVue = new VueSaisieRepresentation();
        $this->vue = $laVue;
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Nouvelle Representation");
        // En création, on affiche un formulaire vide
        $unLieu = new Lieu('', '', '', '');
        $unGroupe = new Groupe('', '', '', '', '', '', '');
        /* @var Representation $uneRep */
        $uneRep = new Representation('', $unLieu, $unGroupe, '', '', '');
        $this->vue->setUneRepresentation($uneRep);
        
        Bdd::connecter();
        $lesGroupes = GroupeDAO::getAll();
        $this->vue->setLesGroupes($lesGroupes);
        $lesLieux = LieuDAO::getAll();
        $this->vue->setLesLieux($lesLieux);
        
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representation");
        $this->vue->setVersion($this->version);
        $this->vue->afficher();
    }

    /** controleur= Representation & action=validerCreer
     * ajouter une Representation dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        /* @var Representation $uneRep  : récupération du contenu du formulaire et instanciation d'une representation */
        $uneRep = new Representation($_REQUEST['idRepresentation'], LieuDAO::getOneById($_REQUEST['lieu']), GroupeDAO::getOneById($_REQUEST['groupe']),
                $_REQUEST['date'], $_REQUEST['hDebut'], $_REQUEST['hFin']);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de création (paramètre n°1 = true)
        $this->verifierDonneesRep($uneRep, true);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer la representation
            RepresentationDao::insert($uneRep);
            // revenir à la liste des representation
            header("Location: index.php?controleur=representation&action=consulter");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de création
            $laVue = new VueSaisieRepresentation();
            $this->vue = $laVue;
            $laVue->setActionRecue("creer");
            $laVue->setActionAEnvoyer("validerCreer");
            $laVue->setMessage("Nouvelle Representation");
            $lesGroupes = GroupeDAO::getAll();
            $this->vue->setLesGroupes($lesGroupes);
            $lesLieux = LieuDAO::getAll();
            $this->vue->setLesLieux($lesLieux);
            $laVue->setUneRepresentation($uneRep);
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - representation");
            $laVue->setVersion($this->version);
            $this->vue->afficher();
        }
    }

    /** controleur= representation & action=modifier $ id=identifiant d'une Representation à modifier
     * Afficher le formulaire de modification d'une Representation     */
    public function modifier() {
        $idRep = $_GET["id"];
        $laVue = new VueSaisieRepresentation();
        $this->vue = $laVue;
        // Lire dans la BDD les données du Groupe à modifier
        Bdd::connecter();
        /* @var Groupe $leGroupe */
        $laRepresentation = RepresentationDao::getOneById($idRep);
        $this->vue->setUneRepresentation($laRepresentation);
        $lesGroupes = GroupeDAO::getAll();
        $this->vue->setLesGroupes($lesGroupes);
        $lesLieux = LieuDAO::getAll();
        $this->vue->setLesLieux($lesLieux);
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier la representation : " . $laRepresentation->getIdRepresentation() .".");
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representation");
        $this->vue->setVersion($this->version);
        $this->vue->afficher();
    }

    /** controleur= representation & action=validerModifier
     * modifier une representation dans la base de données d'après la saisie    */
    public function validerModifier() {
        Bdd::connecter();
        /* @var Representation $uneRep  : récupération du contenu du formulaire et instanciation d'une Representation */
        $uneRep = new Representation($_REQUEST['idRepresentation'], LieuDAO::getOneById($_REQUEST['lieu']), GroupeDAO::getOneById($_REQUEST['groupe']),
                $_REQUEST['date'], $_REQUEST['hDebut'], $_REQUEST['hFin']);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        $this->verifierDonneesRep($uneRep, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications pour un groupe
            RepresentationDao::update($uneRep->getIdRepresentation(), $uneRep);
            // revenir à la liste des Groupes
            header("Location: index.php?controleur=Representation&action=consulter");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieRepresentation();
            $this->vue = $laVue;
            $laVue->setUneRepresentation($uneRep);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier la Représentation : " . $uneRep->getIdRepresentation());
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - Representation");
            $laVue->setVersion($this->version);
            $this->vue->afficher();
        }
    }

    /** controleur= Representation & action=supprimer & id=identifiant_Representation
     * Supprimer uneRepresentation d'après son identifiant     */
    public function supprimer() {
        $idRep = $_GET["id"];
        $this->vue = new VueSupprimmerRepresentation();
        // Lire dans la BDD les données du groupe à supprimer
        Bdd::connecter();
        $this->vue->setUneRepresentation(RepresentationDao::getOneById($idRep));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representation");
        $this->vue->setVersion($this->version);
        $this->vue->afficher();
    }

    /** controleur= Representation & action= validerSupprimer
     * supprimer une Representation dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant du groupe à supprimer");
        } else {
            // suppression du groupe d'après son identifiant
            RepresentationDao::delete($_GET["id"]);
        }
        // retour à la liste des Groupes
        header("Location: index.php?controleur=representation&action=consulter");
    }

    /**
     * Vérification des données du formulaire de saisie
     * @param Representation $uneRep à vérifier
     * @param bool $creation : =true si formulaire de création d'un nouveau groupe ; =false sinon
     */
    private function verifierDonneesRep(Representation $uneRep, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
        if (($creation && $uneRep->getIdRepresentation() == "") || $uneRep->getLieu()->getNom() == "" || $uneRep->getGroupe()->getNom() == "" || $uneRep->getHDebut() == "" ||
                $uneRep->getHFin() == "" || $uneRep->getDate() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
        }
        // En cas de création, vérification du format de l'id et de sa non existence
        if ($creation && $uneRep->getIdRepresentation() != "") {
            // Si l'id est constitué d'autres caractères que de lettres non accentuées 
            // et de chiffres, une erreur est générée
            if (!estAlphaNumerique($uneRep->getIdRepresentation())) {
                GestionErreurs::ajouter("L'identifiant doit comporter uniquement des lettres non accentuées et des chiffres");
            } else {
                if (RepresentationDao::isAnExistingId($uneRep->getIdRepresentation())) {
                    GestionErreurs::ajouter("La Representation " . $uneRep->getIdRepresentation() . " existe déjà");
                }
            }
        }
        // Vérification qu'un groupe de même nom n'existe pas déjà (id + nom si création)
        if (RepresentationDao::isAnExistingId($creation, $uneRep->getIdRepresentation())) {
            GestionErreurs::ajouter("La Representation " . $uneRep->getIdRepresentation() . " existe déjà");
        }
 
    }
    
}