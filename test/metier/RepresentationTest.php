<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Lieu Test</title>
    </head>
    <body>
        <?php
        use modele\metier\Lieu;
        use modele\metier\Groupe;
        use modele\metier\Representation;
        require_once __DIR__ . '/../../includes/autoload.inc.php';
        echo "<h2>Test unitaire de la classe métier Représentation</h2>";
        $unGroupe = new Groupe("G001", "TheGroupe", null, null, 25, "Angleterre", "N");
        $unLieu = new Lieu("L01" ,"Nantes", "5 rue de la PPE", 148);
        $uneRepresentation = new Representation("R001", $unLieu, $unGroupe, date('Y/d/m'), 9, 10);
        var_dump($uneRepresentation);
        ?>
    </body>
</html>