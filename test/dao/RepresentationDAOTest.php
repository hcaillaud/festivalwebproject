<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Representation : test</title>
    </head>

    <body>

        <?php

        use modele\metier\Representation;
        use modele\dao\RepresentationDao;
        use modele\dao\LieuDAO;
        use modele\dao\GroupeDAO;
        use modele\dao\Bdd;
        use controleur\Session;

        require_once __DIR__ . '/../../includes/autoload.inc.php';

        $id = 'R001';
        Session::demarrer();
        Bdd::connecter();

        echo "<h2>Test RepresentationDAO</h2>";

        // Test n°1
        echo "<h3>1- Test getOneById</h3>";
        try {
            $objet = RepresentationDao::getOneById($id);
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }

        // Test n°2
        echo "<h3>2- Test getAll</h3>";
        try {
            $lesObjets = RepresentationDao::getAll();
            var_dump($lesObjets);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }

        // Test n°3
        echo "<h3>3- insert</h3>";
        try {
            $id = 'R00X';
            $unLieu = LieuDAO::getOneById("L01");
            $unGroupe = GroupeDAO::getOneById("g001");
            $uneRepresentation = new Representation($id, $unLieu, $unGroupe, date('Y/m/d'), "9", "12");
            
            $ok = RepresentationDao::insert($uneRepresentation);
            if ($ok) {
                echo "<h4>ooo réussite de l'insertion ooo</h4>";
                $objetLu = RepresentationDao::getOneById($id);
                var_dump($objetLu);
            } else {
                echo "<h4>*** échec de l'insertion ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }

        // Test n°3-bis
        echo "<h3>3-bis insert déjà présent</h3>";
        try {
            $unLieu2 = LieuDAO::getOneById("L01");
            $unGroupe2 = GroupeDAO::getOneById("g001");
            
            $uneRepresentation2 = new Representation($id, $unLieu, $unGroupe, date('Y/m/d'), 9, 12);
            
            $ok = RepresentationDao::insert($uneRepresentation2);
            if ($ok) {
                echo "<h4>*** échec du test : l'insertion ne devrait pas réussir  ***</h4>";
                $objetLu = RepresentationDao::getOneById($id);
                var_dump($objetLu);
            } else {
                echo "<h4>ooo réussite du test : l'insertion a logiquement échoué ooo</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>ooo réussite du test : la requête d'insertion a logiquement échoué ooo</h4>" . $e->getMessage();
        }

        // Test n°4
        echo "<h3>4- update</h3>";
        try {
            $uneRepresentation->setHDebut("13");
            $uneRepresentation->setHFin("14");
            $ok = RepresentationDao::update($id, $uneRepresentation);
            if ($ok) {
                echo "<h4>ooo réussite de la mise à jour ooo</h4>";
                $objetLu = RepresentationDao::getOneById($id);
                var_dump($objetLu);
            } else {
                echo "<h4>*** échec de la mise à jour ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }

        // Test n°5
        echo "<h3>5- delete</h3>";
        try {
            $ok = RepresentationDao::delete($id);
            if ($ok) {
                echo "<h4>ooo réussite de la suppression id:", $id, " ooo</h4>";
            } else {
                echo "<h4>*** échec de la suppression ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }

        // Test n°6
        echo "<h3>6- isAnExistingId</h3>";
        try {
            $id = "R002";
            $ok = RepresentationDao::isAnExistingId($id);
            $ok = $ok && !RepresentationDao::isAnExistingId('AZERTY');
            if ($ok) {
                echo "<h4>ooo test réussi l'id:", $id, " existe ooo</h4>";
            } else {
                echo "<h4>*** échec du test ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }

        Bdd::deconnecter();
        Session::arreter();
        ?>


    </body>
</html>
